<?php
namespace Bss\ProductViewEvent\Observer;

/**
 * Class SaveCmsPageObserver
 *
 * Observer for Save cms page event.
 */
class SaveCmsPageObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * SaveCmsPageObserver constructor.
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->messageManager = $messageManager;
    }

    /**
     * Send a message with page's title.
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $message = __("Page Title: %1",$observer->getEvent()->getPage()->getTitle());
        $this->messageManager->addNoticeMessage($message);

        return $this;
    }
}
