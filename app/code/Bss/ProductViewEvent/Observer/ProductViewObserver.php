<?php
namespace Bss\ProductViewEvent\Observer;

/**
 * Class ProductViewObserver
 *
 * Observer for Product View event.
 */
class ProductViewObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\App\ResponseFactory
     */
    protected $responseFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $session;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * ProductViewObserver constructor.
     * @param \Magento\Framework\App\ResponseFactory $responseFactory
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Customer\Model\Session $session
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\UrlInterface $url,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->responseFactory = $responseFactory;
        $this->url = $url;
        $this->session = $session;
        $this->messageManager = $messageManager;
    }

    /**
     * If customer is not logged in, redirect them to login page with error message.
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        if (!$this->session->isLoggedIn()) {
            $message = __("You must login to do this. Hello Intern.");
            $this->messageManager->addErrorMessage($message);
            $redirectUrl = $this->url->getUrl('customer/account/login');
            $this->responseFactory->create()->setRedirect($redirectUrl)->sendResponse();
        }
        return $this;
    }
}
