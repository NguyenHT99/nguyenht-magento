<?php
namespace Bss\ProductViewEvent\Observer;

/**
 * Class AddToCartObserver
 *
 * Observer for Add To Cart event.
 */
class AddToCartObserver implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * AddToCartObserver constructor.
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->messageManager = $messageManager;
    }

    /**
     * Get product's information and send a message with product's quantity.
     *
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $product = $observer->getEvent()->getProduct();
        $message = __("Product Quantity: %1", $product->getExtensionAttributes()->getStockItem()->getQty());

        $this->messageManager->addSuccessMessage($message);

        return $this;
    }
}
