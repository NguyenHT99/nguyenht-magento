define(['jquery','owl'], function($)
{
    return function(config, element)
    {
        $(element).owlCarousel({
            navigation : true,
            autoplay: true,
            autoplayHoverPause: false,
            autoplaySpeed: 2000,
            loop: true,
            smartSpeed: 450,
            items: 4,
            margin: 5
        });
    };
});
