define(['jquery',
        'uiComponent',
        'ko',
        'Magento_Customer/js/customer-data',
        'Magento_Ui/js/modal/modal'
    ], function ($, Component, ko, customerData, modal) {
        'use strict';
        return Component.extend({
            defaults: {
                template: 'Bss_HelloIntern/knockout-template'
            },
            initialize: function () {
                var self = this;
                var before = 0;
                this.subtotalCart = ko.observable('');
                this._super();
                customerData.get('cart').subscribe(function (result) {
                    if (parseFloat(result.subtotalAmount) > before) {
                        self.subtotalCart($.mage.__("Cart total: %1 (Before: %2)")
                            .replace('%1', parseFloat(result.subtotalAmount))
                            .replace('%2', before));
                        var options = {
                            type: 'popup',
                            responsive: true,
                            innerScroll: true,
                            title: $.mage.__('Add to Cart'),
                            buttons: [{
                                text: $.mage.__('Continue'),
                                class: '',
                                click: function () {
                                    this.closeModal();
                                }
                            }]
                        };
                        var popup = modal(options, $('#popup-modal'));
                        $("#popup-modal").modal("openModal");
                    }
                    before = parseFloat(result.subtotalAmount);
                });
            },
        });
    }
);
