<?php
namespace Bss\HelloIntern\Model\ResourceModel\Intern;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'id';

    protected function _construct()
    {
        $this->_init(
            \Bss\HelloIntern\Model\Intern::class,
            \Bss\HelloIntern\Model\ResourceModel\Intern::class
        );
    }
}
