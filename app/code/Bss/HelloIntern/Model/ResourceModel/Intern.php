<?php
namespace Bss\HelloIntern\Model\ResourceModel;

class Intern extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected $_isPkAutoIncrement = false;

    protected function _construct()
    {
        $this->_init('internship', 'id');
    }
}
