<?php
namespace Bss\HelloIntern\Model;

/**
 * Class Intern
 * Provide function to get basic intern's data.
 */
class Intern extends \Magento\Framework\Model\AbstractModel implements \Bss\HelloIntern\Api\Data\InternInterface
{
    /**
     * Carrier constructor.
     */
    protected function _construct()
    {
        $this->_init(\Bss\HelloIntern\Model\ResourceModel\Intern::class);
    }

    /**
     * Get Intern's ID.
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData('id');
    }

    /**
     * Get Intern's Name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->getData('name');
    }

    /**
     * Get Intern's avatar link.
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->getData('avatar');
    }

    /**
     * Get Intern's date of birth in string.
     *
     * @return string
     */
    public function getDob()
    {
        return $this->getData('dob');
    }

    /**
     * Get Intern's description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->getData('description');
    }
}
