<?php
namespace Bss\HelloIntern\Model;

/**
 * Class InternRepository
 * Provide save/delete/get function for Intern.
 */
class InternRepository implements \Bss\HelloIntern\Api\InternRepositoryInterface
{
    /**
     * @var ResourceModel\Intern
     */
    protected $resource;

    /**
     * @var InternFactory
     */
    protected $internFactory;

    /**
     * InternRepository constructor.
     *
     * @param InternFactory $internFactory
     * @param ResourceModel\Intern $resource
     */
    public function __construct(
        \Bss\HelloIntern\Model\InternFactory $internFactory,
        \Bss\HelloIntern\Model\ResourceModel\Intern $resource
    ) {
        $this->resource = $resource;
        $this->internFactory = $internFactory;
    }

    /**
     * Save an Intern.
     *
     * @param \Bss\HelloIntern\Api\Data\InternInterface $intern
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Bss\HelloIntern\Api\Data\InternInterface $intern)
    {
        try {
            $this->resource->save($intern);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotSaveException(__($exception->getMessage()));
        }
    }

    /**
     * Delete an Intern.
     *
     * @param \Bss\HelloIntern\Api\Data\InternInterface $intern
     * @throws \Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(\Bss\HelloIntern\Api\Data\InternInterface $intern)
    {
        try {
            $this->resource->delete($intern);
        } catch (\Exception $exception) {
            throw new \Magento\Framework\Exception\CouldNotDeleteException(__($exception->getMessage()));
        }
    }

    /**
     * @param int $id
     * @return \Bss\HelloIntern\Api\Bss\HelloIntern\Model\Intern|Intern
     */
    public function get($id)
    {
        return $this->internFactory->create()->load($id);
    }
}
