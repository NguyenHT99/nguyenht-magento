<?php
namespace Bss\HelloIntern\Api\Data;

interface InternInterface
{

    /**
     * Get Intern's ID.
     *
     * @return int
     */
    public function getId();

    /**
     * Get Intern's Name.
     *
     * @return string
     */
    public function getName();

    /**
     * Get Intern's avatar link.
     *
     * @return string
     */
    public function getAvatar();

    /**
     * Get Intern's date of birth in string.
     *
     * @return string
     */
    public function getDob();

    /**
     * Get Intern's description.
     *
     * @return string
     */
    public function getDescription();
}
