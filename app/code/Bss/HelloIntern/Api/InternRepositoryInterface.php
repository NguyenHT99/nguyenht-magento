<?php
namespace Bss\HelloIntern\Api;

interface InternRepositoryInterface
{

    /**
     * Save an Intern.
     *
     * @param Data\InternInterface $intern
     * @throws Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(Data\InternInterface $intern);

    /**
     * Delete an Intern.
     *
     * @param Data\InternInterface $intern
     * @throws Magento\Framework\Exception\CouldNotDeleteException
     */
    public function delete(Data\InternInterface $intern);

    /**
     * Get an Object with ID.
     *
     * @param $id
     * @return Bss\HelloIntern\Model\Intern
     */
    public function get($id);
}
