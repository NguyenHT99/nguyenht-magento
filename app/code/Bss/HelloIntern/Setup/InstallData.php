<?php
namespace Bss\HelloIntern\Setup;

class InstallData implements \Magento\Framework\Setup\InstallDataInterface
{
    protected $_internFactory;

    public function __construct(\Bss\HelloIntern\Model\InternFactory $internFactory)
    {
        $this->_internFactory = $internFactory;
    }

    public function install(
        \Magento\Framework\Setup\ModuleDataSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $data = [
            ['id'=>1,
                'name'=>'Lee Baby',
                'avatar'=>'https://static.wikia.nocookie.net/sonicpokemon/images/b/b2/Psyduck_AG_anime.png',
                'dob'=>'1998-01-15',
                'description'=>'Lover'],
            ['id'=>2,
                'name'=>'Ho Lee Day',
                'avatar'=>'https://static.wikia.nocookie.net/sonicpokemon/images/b/b2/Psyduck_AG_anime.png',
                'dob'=>'1998-03-12',
                'description'=>'Ho Ho Ho'],
            ['id'=>3,
                'name'=>'Army Rican',
                'avatar'=>'https://static.wikia.nocookie.net/sonicpokemon/images/b/b2/Psyduck_AG_anime.png',
                'dob'=>'1998-06-06',
                'description'=>'Country'],
            ['id'=>4,
                'name'=>'Lada GayGy',
                'avatar'=>'https://static.wikia.nocookie.net/sonicpokemon/images/b/b2/Psyduck_AG_anime.png',
                'dob'=>'1998-03-05',
                'description'=>'Girl'],
            ['id'=>5,
                'name'=>'Janim Ikcin',
                'avatar'=>'https://static.wikia.nocookie.net/sonicpokemon/images/b/b2/Psyduck_AG_anime.png',
                'dob'=>'1998-12-12',
                'description'=>'Rapper']
        ];
        foreach ($data as $d) {
            $this->_internFactory->create()->addData($d)->save();
        }
    }
}
