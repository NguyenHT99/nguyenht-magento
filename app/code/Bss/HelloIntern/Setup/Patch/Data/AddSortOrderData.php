<?php
namespace Bss\HelloIntern\Setup\Patch\Data;

/**
 * Add data to column Sort Order.
 */
class AddSortOrderData implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * AddSortOrderData constructor.
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * Get Dependencies.
     *
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get Aliases.
     *
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Add data into column 'sort_order' in table 'internship'.
     *
     * @return void
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        $this->moduleDataSetup->getConnection()->update(
            'internship',
            ['sort_order'=>5]
        );
        $this->moduleDataSetup->getConnection()->update(
            'internship',
            ['sort_order'=>1],
            $where = ['id = ?' => 1]
        );
        $this->moduleDataSetup->getConnection()->update(
            'internship',
            ['sort_order'=>6],
            $where = ['id = ?' => 3]
        );
        $this->moduleDataSetup->getConnection()->update(
            'internship',
            ['sort_order'=>1],
            $where = ['id = ?' => 5]
        );
        $this->moduleDataSetup->getConnection()->update(
            'internship',
            ['sort_order'=>4],
            $where = ['id = ?' => 4]
        );

        $this->moduleDataSetup->endSetup();
    }
}
