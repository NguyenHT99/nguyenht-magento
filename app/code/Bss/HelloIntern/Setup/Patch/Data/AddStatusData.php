<?php
namespace Bss\HelloIntern\Setup\Patch\Data;

/**
 * Add data to column 'Status'.
 */
class AddStatusData implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * AddStatusData constructor.
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * Get Dependencies.
     *
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get Aliases.
     *
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Add data to column 'status' in table 'internship'.
     *
     * @return void
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        $this->moduleDataSetup->getConnection()->update(
            'internship',
            ['status'=>'enabled']
        );
        $this->moduleDataSetup->getConnection()->update(
            'internship',
            ['status'=>'disabled'],
            $where = ['id = ?' => 2]
        );
        $this->moduleDataSetup->getConnection()->update(
            'internship',
            ['status'=>'disabled'],
            $where = ['id = ?' => 6]
        );

        $this->moduleDataSetup->endSetup();
    }
}
