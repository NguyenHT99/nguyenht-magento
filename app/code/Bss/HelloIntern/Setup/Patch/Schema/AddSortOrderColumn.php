<?php
namespace Bss\HelloIntern\Setup\Patch\Schema;

/**
 * Add column 'sort_order'.
 */
class AddSortOrderColumn implements \Magento\Framework\Setup\Patch\SchemaPatchInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * AddSortOrderColumn constructor.
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * Get Dependencies.
     *
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get Aliases.
     *
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Add column 'sort_order' into table 'internship'.
     *
     * @return void
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        $this->moduleDataSetup->getConnection()->addColumn(
            'internship',
            'sort_order',
            [
                'type'=>\Magento\Framework\Db\Ddl\Table::TYPE_INTEGER,
                'length'=>32,
                'nullable'=>true,
                'comment'=>'Sort Order'
            ]
        );

        $this->moduleDataSetup->endSetup();
    }
}
