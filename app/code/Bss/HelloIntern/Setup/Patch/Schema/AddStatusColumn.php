<?php
namespace Bss\HelloIntern\Setup\Patch\Schema;

/**
 * Class for adding new column 'Status'.
 */
class AddStatusColumn implements \Magento\Framework\Setup\Patch\SchemaPatchInterface
{
    /**
     * @var \Magento\Framework\Setup\ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     * AddStatusColumn constructor.
     * @param \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
     */
    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
    }

    /**
     * Get Dependencies.
     *
     * @return array
     */
    public static function getDependencies()
    {
        return [];
    }

    /**
     * Get Aliases.
     *
     * @return array
     */
    public function getAliases()
    {
        return [];
    }

    /**
     * Add new column 'Status' into table 'internship'.
     *
     * @return void
     */
    public function apply()
    {
        $this->moduleDataSetup->startSetup();

        $this->moduleDataSetup->getConnection()->addColumn(
            'internship',
            'status',
            [
                'type'=>\Magento\Framework\Db\Ddl\Table::TYPE_TEXT,
                'length'=>32,
                'nullable'=>false,
                'comment'=>'Status'
            ]
        );

        $this->moduleDataSetup->endSetup();
    }
}
