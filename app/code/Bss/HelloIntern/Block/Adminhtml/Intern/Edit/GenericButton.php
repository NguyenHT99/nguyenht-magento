<?php
namespace Bss\HelloIntern\Block\Adminhtml\Intern\Edit;

class GenericButton
{
    /**
     * @var \Magento\Backend\Block\Widget\Context
     */
    protected $context;

    /**
     * @var \Bss\HelloIntern\Model\InternRepository
     */
    protected $internRepository;

    /**
     * GenericButton constructor.
     * @param \Magento\Backend\Block\Widget\Context $context
     * @param \Bss\HelloIntern\Model\InternRepository $internRepository
     */
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context,
        \Bss\HelloIntern\Model\InternRepository $internRepository
    ) {
        $this->context = $context;
        $this->internRepository = $internRepository;
    }

    /**
     * Get Intern's ID from request param.
     *
     * @return int|null
     */
    public function getInternId()
    {
        try {
            return $this->internRepository->get(
                $this->context->getRequest()->getParam('id')
            )->getId();
        } catch (\Magento\Framework\Exception\NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Get Url.
     *
     * @param string $route
     * @param array $params
     * @return string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
