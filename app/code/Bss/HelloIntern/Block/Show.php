<?php
namespace Bss\HelloIntern\Block;

class Show extends \Magento\Framework\View\Element\Template
{
    protected $internRepository;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Bss\HelloIntern\Model\InternRepository $internRepository
    ) {
        $this->internRepository = $internRepository;
        return parent::__construct($context);
    }

    public function getDataID()
    {
        $id = $this->getRequest()->getParam('id');

        return $this->internRepository->get((int)$id);
    }
}
