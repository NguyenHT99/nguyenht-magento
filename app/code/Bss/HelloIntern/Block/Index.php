<?php
namespace Bss\HelloIntern\Block;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $internFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Bss\HelloIntern\Model\InternFactory $internFactory
    ) {
        $this->internFactory = $internFactory;
        return parent::__construct($context);
    }

    public function getInternCollection()
    {
        return $this->internFactory->create()->getCollection()->addOrder('sort_order', 'ASC');
    }
}
