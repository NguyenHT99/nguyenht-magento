<?php
namespace Bss\HelloIntern\Block\Widget;

/**
 * Class InternList
 *
 * Provide access from Widget to templates, templates to collection.
 */
class InternList extends \Magento\Framework\View\Element\Template implements \Magento\Widget\Block\BlockInterface
{
    /**
     * @var string
     */
    protected $_template = "widget/internlist.phtml";

    /**
     * @var \Bss\HelloIntern\Model\InternFactory
     */
    protected $internFactory;

    /**
     * InternList constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Bss\HelloIntern\Model\InternFactory $internFactory
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Bss\HelloIntern\Model\InternFactory $internFactory
    ) {
        $this->internFactory = $internFactory;
        return parent::__construct($context);
    }

    /**
     * Get Intern Collection.
     * If 'countIntern' config is a number, get the same amount of rows.
     * Else, get all rows.
     *
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getInternCollection()
    {
        $internCount = $this->getData("countIntern");

        $collection = $this->internFactory->create()->getCollection();

        if (is_numeric($internCount)) {
            $collection
                ->setPageSize((int) $internCount)
                ->setCurPage(1)
                ->load();
        }

        return $collection;
    }
}
