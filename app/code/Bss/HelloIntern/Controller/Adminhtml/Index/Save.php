<?php
namespace Bss\HelloIntern\Controller\Adminhtml\Index;

use Magento\Framework\App\ResponseInterface;

class Save implements \Magento\Framework\App\Action\HttpPostActionInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Bss\HelloIntern\Model\InternRepository
     */
    protected $internRepository;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;

    /**
     * Save constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Bss\HelloIntern\Model\InternRepository $internRepository
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\Controller\ResultFactory $resultFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Bss\HelloIntern\Model\InternRepository $internRepository,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Controller\ResultFactory $resultFactory
    ) {
        $this->request = $context->getRequest();
        $this->internRepository = $internRepository;
        $this->messageManager = $messageManager;
        $this->resultFactory = $resultFactory;
    }

    /**
     * Save an intern.
     *
     * @return ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $params = $this->request->getParams();
            $id = $this->request->getParam('id');
            $type = $this->request->getParam('back');
            if ($id) {
                $intern = $this->internRepository->get($id);
            }
            $intern->setData($params);
            $this->internRepository->save($intern);
            $this->messageManager->addSuccessMessage(__("You saved the intern"));
            if ($type !== 'close') {
                return $this->resultFactory
                    ->create($this->resultFactory::TYPE_REDIRECT)
                    ->setPath('*/*/edit', ['id' => $intern->getId()]);
            }
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__("There was an error saving the article."));
        }
        return $this->resultFactory->create($this->resultFactory::TYPE_REDIRECT)->setPath('*/*');
    }
}
