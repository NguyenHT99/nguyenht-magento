<?php
namespace Bss\HelloIntern\Controller\Adminhtml\Index;

class Delete implements \Magento\Framework\App\Action\HttpPostActionInterface
{
    /**
     * @var \Magento\Framework\App\RequestInterface
     */
    protected $request;

    /**
     * @var \Bss\HelloIntern\Model\InternRepository
     */
    protected $internRepository;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Framework\Controller\ResultFactory
     */
    protected $resultFactory;

    /**
     * Delete constructor.
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Bss\HelloIntern\Model\InternRepository $internRepository
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Framework\Controller\ResultFactory $resultFactory
     */
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Bss\HelloIntern\Model\InternRepository $internRepository,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Controller\ResultFactory $resultFactory
    ) {
        $this->request = $context->getRequest();
        $this->internRepository = $internRepository;
        $this->messageManager = $messageManager;
        $this->resultFactory = $resultFactory;
    }

    /**
     * Delete an intern using ID, then redirect user to current page.
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        try {
            $id = $this->request->getParam('id');
            $this->internRepository->delete($this->internRepository->get($id));
            $this->messageManager->addSuccessMessage(__("You deleted the intern."));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        }
        return $this->resultFactory->create($this->resultFactory::TYPE_REDIRECT)->setPath('*/*');
    }
}
