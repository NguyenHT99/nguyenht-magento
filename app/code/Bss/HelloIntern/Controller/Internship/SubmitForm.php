<?php
namespace Bss\HelloIntern\Controller\Internship;

class SubmitForm extends \Magento\Framework\App\Action\Action
{
    protected $internFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Bss\HelloIntern\Model\InternFactory $internFactory
    ) {
        $this->internFactory = $internFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $request = $this->getRequest()->getPost();
        $intern = $this->internFactory->create();

        $data = ['id'=>$request['id'],
            'name'=>$request['name'],
            'avatar'=>$request['avatar'],
            'dob'=>$request['dob'],
            'description'=>$request['description']];
        $intern->setData($data);

        $intern->save();

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('hellointern/internship/');

        return $resultRedirect;
    }
}
