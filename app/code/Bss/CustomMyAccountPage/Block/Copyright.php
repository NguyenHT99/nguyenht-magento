<?php
namespace Bss\CustomMyAccountPage\Block;

class Copyright extends \Magento\Framework\View\Element\Template
{
    public function getCopyright() {
        return __("Copyright");
    }
}
