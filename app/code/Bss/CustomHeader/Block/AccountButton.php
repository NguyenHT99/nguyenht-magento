<?php
namespace Bss\CustomHeader\Block;

use Magento\Framework\View\Element\Template;

class AccountButton extends \Magento\Framework\View\Element\Template {
    protected $session;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        array $data = [],
        \Magento\Customer\Model\Session $session
    )
    {
        parent::__construct($context, $data);
        $this->session = $session;
    }

    public function getSession() {
        return $this->session;
    }
}
