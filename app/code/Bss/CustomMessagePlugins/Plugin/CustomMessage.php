<?php
namespace Bss\CustomMessagePlugins\Plugin;

/**
 * Class CustomMessage
 * Add prefix to all of website's message.
 */
class CustomMessage
{
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $session;

    /**
     * CustomMessage constructor.
     * @param \Magento\Customer\Model\Session $session
     */
    public function __construct(\Magento\Customer\Model\Session $session)
    {
        $this->session = $session;
    }

    /**
     * Add customer's name before message.
     *
     * @param \Magento\Framework\Message\ManagerInterface $subject
     * @param \Magento\Framework\Message\MessageInterface $message
     * @param null $group
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeAddMessage(
        \Magento\Framework\Message\ManagerInterface $subject,
        \Magento\Framework\Message\MessageInterface $message,
        $group = null
    ) {
        $text = $message->getText();
        $customerName = "";

        if ($this->session->isLoggedIn()) {
            $customerName = $this->session->getCustomer()->getName();
        } else {
            $customerName = "guest";
        }

        $text = "Dear ".$customerName.", ".$text;
        $message->setText($text);

        return [$message, $group];
    }
}
