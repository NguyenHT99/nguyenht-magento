<?php
namespace Bss\CustomMessagePlugins\Plugin;

use Magento\Catalog\Model\Product;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Show Product's Quantity on message.
 */
class ProductQuantity
{
    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * @var \Magento\Catalog\Model\ProductRepository
     */
    protected $productRepository;

    /**
     * @var \Magento\Checkout\Model\CartFactory
     */
    protected $cartFactory;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * ProductQuantity constructor.
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     * @param \Magento\Catalog\Model\ProductRepository $productRepository
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     */
    public function __construct(
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Catalog\Model\ProductRepository $productRepository,
        \Magento\Store\Model\StoreManagerInterface $storeManager
    ) {
        $this->messageManager = $messageManager;
        $this->productRepository = $productRepository;
        $this->storeManager = $storeManager;

    }

    /**
     * Send a message showing Product's quantity.
     *
     * @param \Magento\Checkout\Model\Cart $subject
     * @param callable $proceed
     * @param $productInfo
     * @param null $requestInfo
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function aroundAddProduct(
        \Magento\Checkout\Model\Cart $subject,
        callable $proceed,
        $productInfo,
        $requestInfo = null
    ) {
        $proceed($productInfo, $requestInfo);
        $product = null;
        if ($productInfo instanceof Product) {
            $product = $productInfo;
            if (!$product->getId()) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("The product wasn't found. Verify the product and try again.")
                );
            }
        } elseif (is_int($productInfo) || is_string($productInfo)) {
            $storeId = $this->storeManager->getStore()->getId();
            try {
                $product = $this->productRepository->getById($productInfo, false, $storeId);
            } catch (NoSuchEntityException $e) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __("The product wasn't found. Verify the product and try again."),
                    $e
                );
            }
        } else {
            throw new \Magento\Framework\Exception\LocalizedException(
                __("The product wasn't found. Verify the product and try again.")
            );
        }

        $message = "Product Quantity: ".$product->getExtensionAttributes()->getStockItem()->getQty();
        $this->messageManager->addSuccessMessage($message);
    }
}
