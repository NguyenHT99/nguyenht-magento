<?php
namespace Bss\CustomMessagePlugins\Plugin;

/**
 * Redirect Catalog Product View request.
 */
class ProductRedirect
{
    /**
     * @var \Magento\Framework\App\ResponseFactory
     */
    protected $responseFactory;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $url;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $session;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * ProductRedirect constructor.
     * @param \Magento\Framework\App\ResponseFactory $responseFactory
     * @param \Magento\Framework\UrlInterface $url
     * @param \Magento\Customer\Model\Session $session
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(
        \Magento\Framework\App\ResponseFactory $responseFactory,
        \Magento\Framework\UrlInterface $url,
        \Magento\Customer\Model\Session $session,
        \Magento\Framework\Message\ManagerInterface $messageManager
    ) {
        $this->responseFactory = $responseFactory;
        $this->url = $url;
        $this->session = $session;
        $this->messageManager = $messageManager;
    }

    /**
     * Redirect customer to login page if they're not logged in.
     *
     * @param \Magento\Catalog\Helper\Product\View $subject
     * @param callable $process
     * @param ...$args
     */
    public function aroundPrepareAndRender(
        \Magento\Catalog\Helper\Product\View $subject,
        callable $process,
        ...$args
    ) {
        if (!$this->session->isLoggedIn()) {
            $message = "You must login to do this.";
            $this->messageManager->addErrorMessage($message);
            $redirectUrl = $this->url->getUrl('customer/account/login');
            $this->responseFactory->create()->setRedirect($redirectUrl)->sendResponse();
        } else {
            $process(...$args);
        }
        return $this;
    }
}
