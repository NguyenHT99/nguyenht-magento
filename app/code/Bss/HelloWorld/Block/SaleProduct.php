<?php
namespace Bss\HelloWorld\Block;

 class SaleProduct extends \Magento\Framework\View\Element\Template
 {
     protected $productCollectionFactory;
     protected $imageBuilderFactory;
     protected $reviewRenderer;

     public function __construct(
         \Magento\Catalog\Block\Product\Context $context,
         array $data = [],
         \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
         \Magento\Catalog\Block\Product\ImageBuilderFactory $imageBuilderFactory
     )
     {
         $this->productCollectionFactory = $productCollectionFactory;
         $this->imageBuilderFactory = $imageBuilderFactory;
         $this->reviewRenderer = $context->getReviewRenderer();
         parent::__construct($context, $data);
     }

     public function getProductList() {
         $collection = $this->productCollectionFactory->create();
         $collection->addAttributeToSelect('*');
         $collection->addFinalPrice()
             ->addMinimalPrice()
             ->getSelect()
             ->where('price_index.final_price < price_index.price');
         return $collection;
     }

     public function getImage($product, $imageId)
     {
         return $this->imageBuilderFactory->create()->setProduct($product)
             ->setImageId($imageId)
             ->create();
     }

     public function getProductPriceHtml(
         \Magento\Catalog\Model\Product $product,
         $priceType,
         $renderZone = \Magento\Framework\Pricing\Render::ZONE_ITEM_LIST,
         array $arguments = []
     ) {
         if (!isset($arguments['zone'])) {
             $arguments['zone'] = $renderZone;
         }

         /** @var \Magento\Framework\Pricing\Render $priceRender */
         $priceRender = $this->getLayout()->getBlock('product.price.render.default');
         $price = '';

         if ($priceRender) {
             $price = $priceRender->render($priceType, $product, $arguments);
         }
         return $price;
     }

     public function getReviewsSummaryHtml(
         \Magento\Catalog\Model\Product $product,
         $templateType = false,
         $displayIfNoReviews = false
     ) {
         return $this->reviewRenderer->getReviewsSummaryHtml($product, $templateType, $displayIfNoReviews);
     }
 }
