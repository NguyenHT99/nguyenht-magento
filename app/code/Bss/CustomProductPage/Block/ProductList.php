<?php
namespace Bss\CustomProductPage\Block;

class ProductList extends \Magento\Framework\View\Element\Template
{
    protected $categoryFactory;
    protected $imageBuilderFactory;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Catalog\Block\Product\ImageBuilderFactory $imageBuilderFactory,
        array $data = []
    )
    {
        $this->categoryFactory = $categoryFactory;
        $this->imageBuilderFactory = $imageBuilderFactory;
        parent::__construct($context, $data);
    }

    public function getProductsByCategory($id)
    {
        $category = $this->categoryFactory->create()->load($id)->getProductCollection()->addAttributeToSelect('*');
        return $category;
    }

    public function getImage($product, $imageId)
    {
        return $this->imageBuilderFactory->create()->setProduct($product)
            ->setImageId($imageId)
            ->create();
    }

    public function getProductPriceHtml(
        \Magento\Catalog\Model\Product $product,
        $priceType,
        $renderZone = \Magento\Framework\Pricing\Render::ZONE_ITEM_LIST,
        array $arguments = []
    ) {
        if (!isset($arguments['zone'])) {
            $arguments['zone'] = $renderZone;
        }

        /** @var \Magento\Framework\Pricing\Render $priceRender */
        $priceRender = $this->getLayout()->getBlock('product.price.render.default');
        $price = '';

        if ($priceRender) {
            $price = $priceRender->render($priceType, $product, $arguments);
        }
        return $price;
    }

}
