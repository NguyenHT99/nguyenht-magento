<?php
namespace Bss\HelloWord\Controller\Forward;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $forwardFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\ForwardFactory $forwardFactory
    ) {
        $this->forwardFactory = $forwardFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->forwardFactory->create();
        $result->setController('index')
            ->setModule('cms')
            ->forward('index');
        return $result;
    }
}
