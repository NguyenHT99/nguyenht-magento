<?php
namespace Bss\HelloWord\Controller\Json;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $jsonFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonFactory
    ) {
        $this->jsonFactory = $jsonFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        $result = $this->jsonFactory->create();
        $param = [
            'Name' => "Ho Trong Nguyen",
            'Age' => '22',
            'Birthday' => '29-04-1999',
            'Information' => 'Test Json',
            'Image' => 'https://static.wikia.nocookie.net/sonicpokemon/images/b/b2/Psyduck_AG_anime.png'
        ];
        return $result->setData($param);
    }
}
