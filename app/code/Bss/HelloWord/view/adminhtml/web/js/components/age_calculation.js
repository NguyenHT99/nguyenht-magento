define([
        'Magento_Ui/js/form/element/abstract',
        'ko'
    ], function (abtract, ko) {
        'use strict';

        return abtract.extend({
            defaults: {
               dob: ko.observable()
            },

            initObservable: function () {
                this._super();
                let self = this;
                this.dob.subscribe(function (dob) {
                    var d = new Date();
                    var y = d.getFullYear();

                    var d2 = new Date(dob);
                    var b = d2.getFullYear();

                    var age = y - b;
                    self.value(age);
                });

                return this;
            }
        })
    }
);
