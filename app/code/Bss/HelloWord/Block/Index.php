<?php
namespace Bss\HelloWord\Block;

class Index extends \Magento\Framework\View\Element\Template
{
    public function getName()
    {
        return $this->_scopeConfig->getValue('about_section/info_group/hw_name_field');
    }
    public function getImage()
    {
        return $this->_scopeConfig->getValue('about_section/info_group/hw_image_field');
    }
    public function getAge()
    {
        return $this->_scopeConfig->getValue('about_section/info_group/hw_age_field');
    }
    public function getDob()
    {
        return $this->_scopeConfig->getValue('about_section/info_group/hw_dob_field');
    }
    public function getInfo()
    {
        return $this->_scopeConfig->getValue('about_section/info_group/hw_info_field');
    }
}
