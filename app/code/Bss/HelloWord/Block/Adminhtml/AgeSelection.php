<?php
namespace Bss\HelloWord\Block\Adminhtml;

class AgeSelection implements \Magento\Framework\Data\OptionSourceInterface
{

    public function toOptionArray()
    {
        $arr = array();
        for ($i = 0; $i < 100; $i++) {
            $arr[] = ['value' => $i, 'label' => $i];
        }
        return $arr;
    }
}
