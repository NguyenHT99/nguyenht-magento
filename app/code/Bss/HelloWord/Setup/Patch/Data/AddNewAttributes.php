<?php
namespace Bss\HelloWord\Setup\Patch\Data;


class AddNewAttributes implements \Magento\Framework\Setup\Patch\DataPatchInterface
{
    protected $moduleDataSetup;
    protected $eavSetupFactory;

    public function __construct(
        \Magento\Framework\Setup\ModuleDataSetupInterface $moduleDataSetup,
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    ) {
        $this->moduleDataSetup = $moduleDataSetup;
        $this->eavSetupFactory = $eavSetupFactory;
    }

    public static function getDependencies()
    {
        return [];
    }

    public function getAliases()
    {
        return [];
    }

    public function apply()
    {
        $eavSetup = $this->eavSetupFactory->create(['setup' => $this->moduleDataSetup]);

        $eavSetup->addAttribute(
            'catalog_product',
            'helloproduct',
            [
                'type' => 'text',
                'label' => 'Hello Product',
                'input' => 'text',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL
            ]
        );
        $eavSetup->addAttribute(
            'customer',
            'hellocustomer',
            [
                'type' => 'text',
                'label' => 'Hello Customer',
                'input' => 'text',
                'global' => \Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface::SCOPE_GLOBAL
            ]
        );
    }
}
